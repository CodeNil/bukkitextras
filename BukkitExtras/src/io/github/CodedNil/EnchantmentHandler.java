package io.github.CodedNil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class EnchantmentHandler {
	public static List<List<?>> enchants = new ArrayList<List<?>>();

	public static void registerEnchantment(String name, String itemType,
			int chance, int minlvl, int maxlvl) {
		enchants.add(Arrays.asList(name, itemType, chance, minlvl, maxlvl));
	}

	public static void EnchantEvent(Player player, ItemStack item, int cost) {
		for (int i = 0; i < enchants.size(); i++) {
			List<?> enchant = enchants.get(i);
			if (Util.isA(item.getType(), enchant.get(1).toString())) {
				double random = Math.random() * 100;
				if (random <= Integer.parseInt(enchant.get(2).toString())) {
					if (cost >= Integer.parseInt(enchant.get(3).toString())) {
						if (player.hasPermission("bukkitextras.customenchants."
								+ enchant.get(0).toString().toLowerCase())) {
							int maxlvl = (Integer.parseInt(enchant.get(4)
									.toString()) - 1);
							int level = (int) Math.round(Math.random() * maxlvl
									+ 1);
							EnchantItem(item, enchant.get(0).toString(), level);
						}
					}
				}
			}
		}
	}

	public static void EnchantItem(ItemStack item, String enchant, int level) {
		ItemMeta itemMeta = item.getItemMeta();
		List<String> lore = new ArrayList<String>();
		if (itemMeta.hasLore()) {
			lore = itemMeta.getLore();
		}
		lore.add("�7" + enchant + " " + numToLevel(level));
		itemMeta.setLore(lore);
		item.setItemMeta(itemMeta);
	}

	public static void RemoveEnchant(ItemStack item, String string) {
		if (!item.hasItemMeta())
			return;
		ItemMeta itemMeta = item.getItemMeta();
		List<String> lore = itemMeta.getLore();
		if (!itemMeta.hasLore())
			return;
		for (int i = 0; i < lore.size(); i++) {
			if (lore.get(i).toLowerCase().contains(string.toLowerCase())) {
				lore.remove(i);
				itemMeta.setLore(lore);
				item.setItemMeta(itemMeta);
				if (hasCustomEnchant(string, item))
					RemoveEnchant(item, string);
				return;
			}
		}
	}

	public static boolean isEnchantValid(String string) {
		for (int i = 0; i < enchants.size(); i++) {
			List<?> enchant = enchants.get(i);
			String enchantname = enchant.get(0).toString();
			if (enchantname.equals(string))
				return true;
		}
		return false;
	}

	public static boolean hasCustomEnchant(String name, ItemStack item) {
		if (!item.hasItemMeta())
			return false;
		ItemMeta itemMeta = item.getItemMeta();
		List<String> lore = itemMeta.getLore();
		if (!itemMeta.hasLore())
			return false;
		for (int i = 0; i < lore.size(); i++) {
			if (lore.get(i).toLowerCase().contains(name.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	public static int getCustomEnchantLevel(String name, ItemStack item) {
		if (!item.hasItemMeta())
			return 0;
		ItemMeta itemMeta = item.getItemMeta();
		List<String> lore = itemMeta.getLore();
		for (int i = 0; i < lore.size(); i++) {
			if (lore.get(i).toLowerCase().contains(name.toLowerCase())) {
				String split = lore.get(i).split(" ")[1];
				return levelToNum(split);
			}
		}
		return 0;
	}

	public static int levelToNum(String level) {
		if (level.equals("I"))
			return 1;
		else if (level.equals("II"))
			return 2;
		else if (level.equals("III"))
			return 3;
		else if (level.equals("IV"))
			return 4;
		else if (level.equals("V"))
			return 5;
		return 0;
	}

	public static String numToLevel(int num) {
		if (num == 1)
			return "I";
		else if (num == 2)
			return "II";
		else if (num == 3)
			return "III";
		else if (num == 4)
			return "IV";
		else if (num == 5)
			return "V";
		return "";
	}
}